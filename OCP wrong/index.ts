import { CoffeeShop } from "./CoffeeShop";
import { Menu } from "./Menu";
import { Product } from "./Product";

const products: Array<Product> = [
    {'id': 1, 'name': 'Negro', 'price': 25},
    {'id': 2, 'name': 'Latte', 'price': 35},
    {'id': 3, 'name': 'Capuc', 'price': 45}
]

let shop = new CoffeeShop();

shop.addProducts(products);

let menu = new Menu();

console.log(menu.viewMenu(shop.getProducts(), false));

console.log(menu.viewMenu(shop.getProducts(), true));
