"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoffeeShop = void 0;
class CoffeeShop {
    constructor() {
        this._products = [];
    }
    addProducts(products) {
        this._products = products;
    }
    getProducts() {
        return this._products;
    }
    removeProduct(id) {
        //Logic to remove a product
    }
}
exports.CoffeeShop = CoffeeShop;
//# sourceMappingURL=CoffeeShop.js.map