"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Menu = void 0;
class Menu {
    viewMenu(products, json) {
        if (json) {
            return JSON.stringify(products);
        }
        else {
            let menu;
            menu = "ID\tName\tPrice\n";
            menu = menu + "=======================\n";
            products.forEach(product => {
                menu = menu + product.id + "\t" + product.name + "\t" + product.price + "\n";
            });
            return menu;
        }
    }
}
exports.Menu = Menu;
//# sourceMappingURL=Menu.js.map