"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const CoffeeShop_1 = require("./CoffeeShop");
const Menu_1 = require("./Menu");
const products = [
    { 'id': 1, 'name': 'Negro', 'price': 25 },
    { 'id': 2, 'name': 'Latte', 'price': 35 },
    { 'id': 3, 'name': 'Capuc', 'price': 45 }
];
let shop = new CoffeeShop_1.CoffeeShop();
shop.addProducts(products);
let menu = new Menu_1.Menu();
console.log(menu.viewMenu(shop.getProducts(), false));
console.log(menu.viewMenu(shop.getProducts(), true));
//# sourceMappingURL=index.js.map