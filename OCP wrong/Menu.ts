import { Product } from "./Product";
export class Menu {

    public viewMenu(products: Array<Product>, json: boolean): string {
        if (json) {
            return JSON.stringify(products);
        }
        else {
            let menu: string;
            menu = "ID\tName\tPrice\n";
            menu = menu + "=======================\n";
            products.forEach(product => {
                menu = menu + product.id + "\t" + product.name + "\t" + product.price + "\n"
            })
            return menu;
        }

    }

}