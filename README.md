![SaFe](https://www.freecodecamp.org/news/content/images/size/w2000/2020/08/solid.png)
# SOLID: The First 5 Principles of Object Oriented Design
## Introduction
S.O.L.I.D. is an acronym for the first five object-oriented design (OOD) principles by Robert C. Martin (also known as Uncle Bob).

When the developer builds software following a bad design, the code can become inflexible and more brittle. Small changes in the software can result in bugs. For these reasons, we should follow SOLID Principles.

These principles establish practices that lend to developing software with considerations for **maintaining** and **extending** as the project grows. Adopting these practices can also contribute to avoiding code smells, refactoring code, and Agile or Adaptive software development.

The following 5 concepts make up our SOLID principles:

- **S**ingle Responsibility
- **O**pen/Closed
- **L**iskov Substitution
- **I**nterface Segregation
- **D**ependency Inversion

While some of these words may sound daunting, they can be easily understood with some simple code examples. In the following sections, we'll take a deep dive into what each of these principles means, along with a quick Typescript example to illustrate each one.

Now, the next examples are going to show you the wrong and the right way to follow the Solid Principles, in our particular case I'm going to show you these examples with Typescript. 

## Single Responsability Principle
Single-responsibility Principle (SRP) states:

> A class should have one and only one reason to change, meaning that a class should have only one job.

How does this principle help us to build better software? Let's see a few of its benefits:

- Testing – A class with one responsibility will have far fewer test cases
- Lower coupling – Less functionality in a single class will have fewer dependencies
- Organization – Smaller, well-organized classes are easier to search than monolithic ones

Following the Single Responsibility Principle is important. First of all, because many different teams can work on the same project and edit the same class for different reasons, this could lead to incompatible modules.

Please look at the following code:
```typescript
//index.ts
import { CoffeeShop } from "./CoffeeShop";
import { Product } from "./Product";

const products: Array<Product> = [
    {'id': 1, 'name': 'Negro', 'price': 25},
    {'id': 2, 'name': 'Latte', 'price': 35},
    {'id': 3, 'name': 'Capuchino', 'price': 45}
]

let shop = new CoffeeShop();

shop.addProducts(products);

console.log(shop.viewMenu());
//expected output

// ID	Name	Price
// =======================
// 1	Negro	25
// 2	Latte	35
// 3	Capuc	45
```

```typescript 
//product.ts
export interface Product {
    id: number;
    name: string;
    price: number;
}
```

```typescript
//Coffeeshop.ts
import { Product } from "./Product";
export class CoffeeShop {
    private _products: Array<Product> = [];

    public addProducts(products: Array<Product>): void {
        this._products = products;
    }

    public getProducts(): Array<Product> {
        return this._products;  
    }

    public removeProduct(id: number): void {
        //Logic to remove a product
    }

    //this method violates SRP because the less cohesion that it has with this class
    //to fix this we should create another class e.g. "class Cart"
    public addProductToCart(id: number, quantity: number): void {
        //Add element to Cart
    }
    //this method violates SRP because the less cohesion that it has with this class
    //to fix this we should create another class e.g. "class Cart"
    public getCartTotal(): void {
        //Add Total Cart
    }
    //this method violates SRP because the less cohesion that it has with this class
    //to fix this we should create another class e.g. "class Menu"
    public viewMenu(): string {
        let menu: string;
        menu = "ID\tName\tPrice\n";
        menu = menu + "=======================\n";
        this._products.forEach(product => {
            menu = menu + product.id + "\t" + product.name + "\t" + product.price + "\n"
        })
        return menu;
    }
}
```
As you can realize the class CoffeShop has three methods that don't follow the SRP, to fix that we should create two classes Cart and View.

So finally the refactored code will be described as below:

```typescript
//Product.ts
export interface Product {
    id: number;
    name: string;
    price: number;
}
```
```typescript
//Cart.ts
export class Cart {
    
    public addProductToCart(id: number, quantity: number): void {
        //Add element to Cart
    }
    
    public getCartTotal(): void {
        //Add Total Cart
    }
    
}
```
```typescript
//Menu.ts
import { Product } from "./Product";
export class Menu {
    public viewMenu(products: Array<Product>): string {
        let menu: string;
        menu = "ID\tName\tPrice\n";
        menu = menu + "=======================\n";
        products.forEach(product => {
            menu = menu + product.id + "\t" + product.name + "\t" + product.price + "\n"
        })
        return menu;
    }
}
```
```typescript
//CoffeeShop.ts
import { Product } from "./Product";
export class CoffeeShop {
    private _products: Array<Product> = [];

    public addProducts(products: Array<Product>): void {
        this._products = products;
    }

    public getProducts(): Array<Product> {
        return this._products;  
    }

    public removeProduct(id: number): void {
        //Logic to remove a product
    }

}
```
```typescript
//index.ts
import { CoffeeShop } from "./CoffeeShop";
import { Menu } from "./Menu";
import { Product } from "./Product";

const products: Array<Product> = [
    {'id': 1, 'name': 'Negro', 'price': 25},
    {'id': 2, 'name': 'Latte', 'price': 35},
    {'id': 3, 'name': 'Capuc', 'price': 45}
]

let shop = new CoffeeShop();

shop.addProducts(products);

let menu = new Menu();

console.log(menu.viewMenu(shop.getProducts()));

//expected output is still

// ID	Name	Price
// =======================
// 1	Negro	25
// 2	Latte	35
// 3	Capuc	45

```

## Open-Closed Principle

Open-closed Principle (S.R.P.) states:

> Objects or entities should be open for extension but closed for modification.

This means that a class should be extendable without modifying the class itself.

Software entities (classes, modules, functions, etc.) should be extendable without actually changing the contents of the class you’re extending. If we could follow this principle strongly enough, it is possible to then modify the behavior of our code without ever touching a piece of the original code.

Now look up the last example, we had our CoffeShop that can generate a text about the products, but now besides that we need also a json output. To achieve this the solution might be like this.

```typescript
//Menu.ts
import { Product } from "./Product";
export class Menu {
    //to get a JSON, the method has a new boolean parameter to deal with the new text format.
    public viewMenu(products: Array<Product>, json: boolean): string {
        //It violates the OCP, because we are modifying the method    
        if (json) {
            return JSON.stringify(products);
        }
        else {
            let menu: string;
            menu = "ID\tName\tPrice\n";
            menu = menu + "=======================\n";
            products.forEach(product => {
                menu = menu + product.id + "\t" + product.name + "\t" + product.price + "\n"
            })
            return menu;
        }

    }

}
```
```typescript
//index.ts
import { CoffeeShop } from "./CoffeeShop";
import { Menu } from "./Menu";
import { Product } from "./Product";

const products: Array<Product> = [
    {'id': 1, 'name': 'Negro', 'price': 25},
    {'id': 2, 'name': 'Latte', 'price': 35},
    {'id': 3, 'name': 'Capuc', 'price': 45}
]

let shop = new CoffeeShop();

shop.addProducts(products);

let menu = new Menu();

console.log(menu.viewMenu(shop.getProducts(), false));

console.log(menu.viewMenu(shop.getProducts(), true));
//expected text output
// ID	Name	Price
// =======================
// 1	Negro	25
// 2	Latte	35
// 3	Capuc	45

//expected json output
//[{"id":1,"name":"Negro","price":25},{"id":2,"name":"Latte","price":35},{"id":3,"name":"Capuc","price":45}]

```
It is violating OCP because:
- First it is modifyng a method
- Second what happen if in the future the application needs a HTML output, I think that the method is going to be modified once again.

Let's Fix it.

```typescript
// Menu.ts
import { IOutputFormatter } from "./IOutputFormatter";
import { Product } from "./Product";
export class Menu {
    public viewMenu(products: Array<Product>, outputFormatter: IOutputFormatter): string {        
        return outputFormatter.output(products);
    }
}

```

```typescript
//IOutputFormatter.ts
import { Product } from "./Product";

export interface IOutputFormatter {
    output(products: Array<Product>): string;
}
```
```typescript
//JsonFormatter.ts
import { IOutputFormatter } from "./IOutputFormatter";
import { Product } from "./Product";

export class JsonFormatter implements IOutputFormatter {
    output(products: Product[]): string {
        return JSON.stringify(products);
    }
}
```

```typescript
//HtmlFormatter.ts
import { IOutputFormatter } from "./IOutputFormatter";
import { Product } from "./Product";

export class HtmlFormatter implements IOutputFormatter {
    output(products: Product[]): string {
        return 'This has to be in HTML Format ';
    }

}
```

```typescript
//TextFormatter.ts
import { IOutputFormatter } from "./IOutputFormatter";
import { Product } from "./Product";

export class TextFormatter implements IOutputFormatter {
    output(products: Product[]): string {
        let menu: string;
        menu = "ID\tName\tPrice\n";
        menu = menu + "=======================\n";
        products.forEach(product => {
            menu = menu + product.id + "\t" + product.name + "\t" + product.price + "\n"
        })
        return menu;
    }

}
```

```typescript
//index.ts
import { CoffeeShop } from "./CoffeeShop";
import { HtmlFormatter } from "./HtmlFormatter";
import { JsonFormatter } from "./JsonFormatter";
import { Menu } from "./Menu";
import { Product } from "./Product";
import { TextFormatter } from "./TextFormatter";

const products: Array<Product> = [
    { 'id': 1, 'name': 'Negro', 'price': 25 },
    { 'id': 2, 'name': 'Latte', 'price': 35 },
    { 'id': 3, 'name': 'Capuc', 'price': 45 }
]

let shop = new CoffeeShop();

shop.addProducts(products);

let menu = new Menu();


console.log(menu.viewMenu(products, new JsonFormatter()));

console.log(menu.viewMenu(products, new TextFormatter()));

console.log(menu.viewMenu(products, new HtmlFormatter()));

//expected output

// console.log(menu.viewMenu(products, new JsonFormatter()));
// [{"id":1,"name":"Negro","price":25},{"id":2,"name":"Latte","price":35},{"id":3,"name":"Capuc","price":45}]

// console.log(menu.viewMenu(products, new TextFormatter()));
// ID	Name	Price
// =======================
// 1	Negro	25
// 2	Latte	35
// 3	Capuc	45

// console.log(menu.viewMenu(products, new HtmlFormatter()));
// This has to be in HTML Format 

```

Now our application supports text, json and html output.

## Liskov Substitution Principle

Liskov Substitution Principle states:

> Let q(x) be a property provable about objects of x of type T. Then q(y) should be provable for objects y of type S where S is a subtype of T.

This means that every subclass or derived class should be substitutable for their base or parent class.

May be this meme help you to undestand it a little, but don't worry there is an example

![Tux, the Linux mascot](https://i2.wp.com/devexperto.com/wp-content/uploads/2016/01/principio-sustitucion-liskov-meme.jpg?resize=750%2C600
)

Let us first understand one example without using the Liskov Substitution Principle in typescript. In the following example, first, we create the Apple class with the method GetColor. Then we create the Orange class which inherits the Apple class as well as overrides the GetColor method of the Apple class. The point is that an Orange cannot be replaced by an Apple, which results in printing the color of apple as Orange as shown in the below example.

```Typescript
//Apple.ts
export class Apple {
    public getColor() {
        return 'red';
    }

}
```
```Typescript
//Apple.ts
import { Apple } from "./Apple";

export class Orange extends Apple {
    //method overriding
    public getColor() {
        return 'orange';
    }

}
```

```Typescript
//Apple.ts
import { Apple } from "./Apple";
import { Orange } from "./Orange";

let apple:Apple = new Orange();

console.log(apple.getColor());
//expected output
//orange
```
Let’s modify the previous example to follow the Liskov Substitution Principle. Here, first, we need a generic base class such as Fruit for both Apple and Orange. Now you can replace the Fruit class object with its subtypes either Apple and Orange and it will behave correctly.

```typescript
//Fruit.ts
export abstract class Fruit {

    public abstract getColor(): string;

}
```
```typescript
//Apple.ts
import { Fruit } from './Fruit'

export class Apple extends Fruit {
    public getColor() {
        return 'red';
    }
}
```
```typescript
//Orange.ts
import { Fruit } from './Fruit'

export class Orange extends Fruit {
    
    public getColor() {
        return 'orange';
    }

}
```
```typescript
//index.ts
import { Apple } from "./Apple";
import { Fruit } from "./Fruit";
import { Orange } from "./Orange";

let fruit: Fruit = new Orange();
console.log(fruit.getColor());

fruit = new Apple();
console.log(fruit.getColor());
//expected output
//orange
//red
```
Now, run the application and it should give the output as expected. Here we are following the Liskov Substitution Principle as we are now able to change the object with its subtype.

Is it pretty hard? well Personally I think that this is the hardest principle. But don't panic at the end of the document you will find awesome examples in the links.


## Interface Segregation Principle
Interface segregation principle states:

> A client should never be forced to implement an interface that it doesn’t use, or clients shouldn’t be forced to depend on methods they do not use.

This rule means that we should break our interfaces into many smaller ones, so they better satisfy the exact needs of our clients.

Similar to the Single Responsibility Principle, the goal of the Interface Segregation Principle is to minimize side consequences and repetition by dividing the software into multiple, independent parts.

Let’s see an example:

```typescript
//IWorker.ts
export interface IWorker{
    work():void;
    sleep():void;
}
```
```typescript
//HumanWorker.ts
import { IWorker } from "./IWorker";

export class HumanWorker implements IWorker{
    work(): void {
        console.log('I just work from 8 am until 17 pm');            
    }
    sleep(): void {
        console.log('I just sleep from 10 pm until 6 m');            
    }
    
}
```
```typescript
//RobotWorker.ts
import { IWorker } from "./IWorker";

export class RobotWorker implements IWorker {
    work(): void {
        console.log("I will work forever, I'm an smart robot bip bip bap");
    }
    sleep(): void {
        throw new Error("Method not implemented.");
    }
}
```
```typescript
// index.ts
import { HumanWorker } from "./HumanWorker";
import { RobotWorker } from "./RobotWorker";

let human: HumanWorker = new HumanWorker();

human.work();
human.sleep();

let robot: RobotWorker = new RobotWorker();

robot.work();
robot.sleep();
//the output has an error since robot.sleep() is not implemented because robots don't sleep as humans

// I just work from 8 am until 17 pm
// I just sleep from 10 pm until 6 m
// I will work until forever, I'm an smart robot bip bip bap
// Process exited with code 1
// > Uncaught Error: Method not implemented.
```

So we can't implement the methods sleep in Robot. Now you might be thinking that you can make something like this:
```typescript
//RobotWorker.ts
import { IWorker } from "./IWorker";

export class RobotWorker implements IWorker {
    work(): void {
        console.log("I will work until forever, I'm an smart robot bip bip bap");
    }
    sleep(): void {
        // it is still violating the ISP
        log('')
    }
}
```
or
```typescript
//RobotWorker.ts
import { IWorker } from "./IWorker";

export class RobotWorker implements IWorker {
    work(): void {
        console.log("I will work until forever, I'm an smart robot bip bip bap");
    }
    sleep(): void {
        // it is still violating the ISP
        // please don't harcoded the method, it works but it is pretty bad
        log('I don\'t sleep bip bap bup')
    }
}
```
In the above code, RobotWorker doesn’t need sleep, but the class has to implement the sleep method because we know that all methods are abstract in the interface. It breaks the Interface Segregation Principle. 

The last both solutions are still violating ISP as well as SRP because the cohesion. Just remember Robots cannot sleep.

Let's fix this segregating the interface

```typescript
//IWork.ts
export interface IWork{
    work():void;    
}
```
```typescript
//ISleep.ts
export interface ISleep{
    sleep():void;
}
```
```typescript
//HumanWorker.ts
import { ISleep } from "./ISleep";
import { IWork } from "./IWork";

export class HumanWorker implements IWork, ISleep{
    work(): void {
        console.log('I just work from 8 am until 17 pm');            
    }
    sleep(): void {
        console.log('I just sleep from 10 pm until 6 m');            
    }
    
}
```

```typescript
//IWork.ts
import { IWork } from "./IWork";

export class RobotWorker implements IWork {
    work(): void {
        console.log("I will work until forever, I'm an smart robot bip bip bap");
    }    
}
```
```typescript
//index.ts
import { HumanWorker } from "./HumanWorker";
import { RobotWorker } from "./RobotWorker";

let human: HumanWorker = new HumanWorker();

human.work();
human.sleep();

let robot: RobotWorker = new RobotWorker();

robot.work();
```

## Dependency Inversion Principle
Dependency inversion principle states:
> Entities must depend on abstractions, not on concretions. It states that the high-level module must not depend on the low-level module, but they should depend on abstractions.

This principle allows for decoupling.

Here is an example of a PasswordReminder that connects to a MySQL database:

```typescript
class MySQLConnection
{
    public connect()
    {
        // handle the database connection
        return 'Database connection';
    }
}
```
```typescript
export class PasswordReminder
{
    private _dbConnection: MySQLConnection;
    // First: Entities must depend on abstractions, not on concretions.
    // This class "PasswordReminder" is depending on another concreted class "MySQLConnection"
    // Thus exits coupling
        
    // Second: It states that the high-level module must not depend on the low-level module, but they should depend on abstractions.
    // In this case "PasswordReminder" represents a high-level module and "MySQLConnection" represents a low-level module
    public constructor(mySQLConnection: MySQLConnection)
    {
        this._dbConnection = mySQLConnection;        
    }
}
```

It might work well, but what happens if we want to change our Database Engine?

if you were to change the database engine, you would also have to edit the PasswordReminder class, and this would violate the open-close principle.

The PasswordReminder class should not care what database your application uses. To address these issues, you can code to an interface since high-level and low-level modules should depend on abstraction:

```typescript
//DBConnectionInterface.ts
export interface DBConnectionInterface
{
     connect():void;
}
```
The interface has a connect method and the MySQLConnection class implements this interface. Also, instead of directly type-hinting MySQLConnection class in the constructor of the PasswordReminder, you instead type-hint the DBConnectionInterface and no matter the type of database your application uses, the PasswordReminder class can connect to the database without any problems and open-close principle is not violated.

```typescript
// MySQLConnection.ts
import { DBConnectionInterface } from "./DBConnectionInterface";

export class MySQLConnection implements DBConnectionInterface
{
    public connect()
    {
        // handle the database connection
        return 'Database connection';
    }
}

```

```typescript
//PasswordReminder.ts
import { DBConnectionInterface } from "./DBConnectionInterface";
import { MySQLConnection } from "./MySqlConnection";

export class PasswordReminder
{
    private _dbConnection: DBConnectionInterface;
 
    public constructor(myConnection: DBConnectionInterface)
    {
        this._dbConnection = myConnection;        
    }
}
```

## Conclusion
In this tutorial, we've taken a deep dive into the SOLID principles of object-oriented design.

We started with a quick bit of SOLID history and the reasons these principles exist.

Letter by letter, we've broken down the meaning of each principle with a quick code example that violates it. We then saw how to fix our code and make it adhere to the SOLID principles.

## Links

### SOLID 

https://www.digitalocean.com/community/conceptual_articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design

https://medium.com/better-programming/solid-principles-simple-and-easy-explanation-f57d86c47a7f#:~:text=SOLID%20Principles%20is%20a%20coding,logical%2C%20and%20easier%20to%20read.

https://www.baeldung.com/solid-principles

### SOLID videos 
https://www.youtube.com/watch?v=_8ecJbcIJKM

### Liskov Substitution

https://dotnettutorials.net/lesson/liskov-substitution-principle/