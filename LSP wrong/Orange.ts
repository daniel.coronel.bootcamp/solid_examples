import { Apple } from "./Apple";

export class Orange extends Apple {
    //method overriding
    public getColor() {
        return 'orange';
    }

}