import { CoffeeShop } from "./CoffeeShop";
import { HtmlFormatter } from "./HtmlFormatter";
import { JsonFormatter } from "./JsonFormatter";
import { Menu } from "./Menu";
import { Product } from "./Product";
import { TextFormatter } from "./TextFormatter";

const products: Array<Product> = [
    { 'id': 1, 'name': 'Negro', 'price': 25 },
    { 'id': 2, 'name': 'Latte', 'price': 35 },
    { 'id': 3, 'name': 'Capuc', 'price': 45 }
]

let shop = new CoffeeShop();

shop.addProducts(products);

let menu = new Menu();


console.log(menu.viewMenu(products, new JsonFormatter()));

console.log(menu.viewMenu(products, new TextFormatter()));

console.log(menu.viewMenu(products, new HtmlFormatter()));

