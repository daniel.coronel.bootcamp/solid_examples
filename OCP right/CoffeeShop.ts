import { Product } from "./Product";
export class CoffeeShop {
    private _products: Array<Product> = [];

    public addProducts(products: Array<Product>): void {
        this._products = products;
    }

    public getProducts(): Array<Product> {
        return this._products;  
    }

    public removeProduct(id: number): void {
        //Logic to remove a product
    }

}