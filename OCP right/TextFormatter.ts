import { IOutputFormatter } from "./IOutputFormatter";
import { Product } from "./Product";

export class TextFormatter implements IOutputFormatter {
    output(products: Product[]): string {
        let menu: string;
        menu = "ID\tName\tPrice\n";
        menu = menu + "=======================\n";
        products.forEach(product => {
            menu = menu + product.id + "\t" + product.name + "\t" + product.price + "\n"
        })
        return menu;
    }

}