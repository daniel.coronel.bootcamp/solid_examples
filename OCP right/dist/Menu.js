"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Menu = void 0;
class Menu {
    viewMenu(products, outputFormatter) {
        return outputFormatter.output(products);
    }
}
exports.Menu = Menu;
//# sourceMappingURL=Menu.js.map