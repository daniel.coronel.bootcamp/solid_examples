"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const CoffeeShop_1 = require("./CoffeeShop");
const HtmlFormatter_1 = require("./HtmlFormatter");
const JsonFormatter_1 = require("./JsonFormatter");
const Menu_1 = require("./Menu");
const TextFormatter_1 = require("./TextFormatter");
const products = [
    { 'id': 1, 'name': 'Negro', 'price': 25 },
    { 'id': 2, 'name': 'Latte', 'price': 35 },
    { 'id': 3, 'name': 'Capuc', 'price': 45 }
];
let shop = new CoffeeShop_1.CoffeeShop();
shop.addProducts(products);
let menu = new Menu_1.Menu();
console.log(menu.viewMenu(products, new JsonFormatter_1.JsonFormatter()));
console.log(menu.viewMenu(products, new TextFormatter_1.TextFormatter()));
console.log(menu.viewMenu(products, new HtmlFormatter_1.HtmlFormatter()));
//# sourceMappingURL=index.js.map