"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TextFormatter = void 0;
class TextFormatter {
    output(products) {
        let menu;
        menu = "ID\tName\tPrice\n";
        menu = menu + "=======================\n";
        products.forEach(product => {
            menu = menu + product.id + "\t" + product.name + "\t" + product.price + "\n";
        });
        return menu;
    }
}
exports.TextFormatter = TextFormatter;
//# sourceMappingURL=TextFormatter.js.map