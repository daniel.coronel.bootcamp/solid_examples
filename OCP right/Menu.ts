import { IOutputFormatter } from "./IOutputFormatter";
import { Product } from "./Product";
export class Menu {
    public viewMenu(products: Array<Product>, outputFormatter: IOutputFormatter): string {        
        return outputFormatter.output(products);
    }
}