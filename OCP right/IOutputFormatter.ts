import { Product } from "./Product";

export interface IOutputFormatter {
    output(products: Array<Product>): string;
}