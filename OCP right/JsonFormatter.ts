import { IOutputFormatter } from "./IOutputFormatter";
import { Product } from "./Product";

export class JsonFormatter implements IOutputFormatter {
    output(products: Product[]): string {
        return JSON.stringify(products);
    }
}