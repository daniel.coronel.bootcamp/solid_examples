import { Fruit } from './Fruit'

export class Apple extends Fruit {
    public getColor() {
        return 'red';
    }
}