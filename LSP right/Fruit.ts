export abstract class Fruit {

    public abstract getColor(): string;

}