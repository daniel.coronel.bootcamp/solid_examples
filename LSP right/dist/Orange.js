"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Orange = void 0;
const Apple_1 = require("./Apple");
class Orange extends Apple_1.Apple {
    //method overriding
    getColor() {
        return 'orange';
    }
}
exports.Orange = Orange;
//# sourceMappingURL=Orange.js.map