import { Apple } from "./Apple";
import { Fruit } from "./Fruit";
import { Orange } from "./Orange";

let fruit: Fruit = new Orange();
console.log(fruit.getColor());

fruit = new Apple();
console.log(fruit.getColor());


Now, run the application and it should give the output as expected. Here we are following the Liskov Substitution Principle as we are now able to change the object with its subtype.
