export class PasswordReminder
{
    private _dbConnection: MySQLConnection;
    // First: Entities must depend on abstractions, not on concretions.
    // This class "PasswordReminder" is depending on another concreted class "MySQLConnection"
    // Thus exits coupling
        
    // Second: It states that the high-level module must not depend on the low-level module, but they should depend on abstractions.
    // In this case "PasswordReminder" represents a high-level module and "MySQLConnection" represents a low-level module
    public constructor(mySQLConnection: MySQLConnection)
    {
        this._dbConnection = mySQLConnection;        
    }
}