import { HumanWorker } from "./HumanWorker";
import { RobotWorker } from "./RobotWorker";

let human: HumanWorker = new HumanWorker();

human.work();
human.sleep();

let robot: RobotWorker = new RobotWorker();

robot.work();

// I just work from 8 am until 17 pm
// I just sleep from 10 pm until 6 m
// I will work until forever, I'm an smart robot bip bip bap
// Process exited with code 1
// > Uncaught Error: Method not implemented.