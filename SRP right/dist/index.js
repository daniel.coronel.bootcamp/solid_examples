"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const CoffeeShop_1 = require("./CoffeeShop");
const products = [
    { 'id': 1, 'name': 'Negro', 'price': 25 },
    { 'id': 2, 'name': 'Latte', 'price': 35 },
    { 'id': 3, 'name': 'Capuc', 'price': 45 }
];
let shop = new CoffeeShop_1.CoffeeShop();
shop.addProducts(products);
console.log(shop.viewMenu());
//# sourceMappingURL=index.js.map