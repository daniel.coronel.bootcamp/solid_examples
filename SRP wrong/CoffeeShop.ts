import { Product } from "./Product";
export class CoffeeShop {
    private _products: Array<Product> = [];

    public addProducts(products: Array<Product>): void {
        this._products = products;
    }

    public getProducts(): Array<Product> {
        return this._products;  
    }

    public removeProduct(id: number): void {
        //Logic to remove a product
    }

    //this method violates SRP because the less cohesion that it has with this class
    //to fix this we should create another class e.g. "class Cart"
    public addProductToCart(id: number, quantity: number): void {
        //Add element to Cart
    }
    //this method violates SRP because the less cohesion that it has with this class
    //to fix this we should create another class e.g. "class Cart"
    public getCartTotal(): void {
        //Add Total Cart
    }
    //this method violates SRP because the less cohesion that it has with this class
    //to fix this we should create another class e.g. "class Menu"
    public viewMenu(): string {
        let menu: string;
        menu = "ID\tName\tPrice\n";
        menu = menu + "=======================\n";
        this._products.forEach(product => {
            menu = menu + product.id + "\t" + product.name + "\t" + product.price + "\n"
        })
        return menu;
    }
}