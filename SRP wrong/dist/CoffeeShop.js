"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoffeeShop = void 0;
class CoffeeShop {
    constructor() {
        this._products = [];
    }
    addProducts(products) {
        this._products = products;
    }
    getProducts() {
        return this._products;
    }
    removeProduct(id) {
        //Logic to remove a product
    }
    //this method violates SRP because the less cohesion that it has with this class
    //to fix this we should create another class e.g. "class Cart"
    addProductToCart(id, quantity) {
        //Add element to Cart
    }
    //this method violates SRP because the less cohesion that it has with this class
    //to fix this we should create another class e.g. "class Cart"
    getCartTotal() {
        //Add Total Cart
    }
    //this method violates SRP because the less cohesion that it has with this class
    //to fix this we should create another class e.g. "class Menu"
    viewMenu() {
        let menu;
        menu = "ID\tName\tPrice\n";
        menu = menu + "=======================\n";
        this._products.forEach(product => {
            menu = menu + product.id + "\t" + product.name + "\t" + product.price + "\n";
        });
        return menu;
    }
}
exports.CoffeeShop = CoffeeShop;
//# sourceMappingURL=CoffeeShop.js.map