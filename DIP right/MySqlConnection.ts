import { DBConnectionInterface } from "./DBConnectionInterface";

export class MySQLConnection implements DBConnectionInterface
{
    public connect()
    {
        // handle the database connection
        return 'Database connection';
    }
}