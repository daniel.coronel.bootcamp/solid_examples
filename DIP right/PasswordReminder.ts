import { DBConnectionInterface } from "./DBConnectionInterface";
import { MySQLConnection } from "./MySqlConnection";

export class PasswordReminder
{
    private _dbConnection: DBConnectionInterface;
 
    public constructor(myConnection: DBConnectionInterface)
    {
        this._dbConnection = myConnection;        
    }
}