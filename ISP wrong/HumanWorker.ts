import { IWorker } from "./IWorker";

export class HumanWorker implements IWorker{
    work(): void {
        console.log('I just work from 8 am until 17 pm');            
    }
    sleep(): void {
        console.log('I just sleep from 10 pm until 6 m');            
    }
    
}