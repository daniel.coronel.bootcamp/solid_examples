import { IWorker } from "./IWorker";

export class RobotWorker implements IWorker {
    work(): void {
        console.log("I will work until forever, I'm an smart robot bip bip bap");
    }
    sleep(): void {
        throw new Error("Method not implemented.");
    }
}