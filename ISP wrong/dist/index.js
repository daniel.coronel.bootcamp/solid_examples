"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const HumanWorker_1 = require("./HumanWorker");
const RobotWorker_1 = require("./RobotWorker");
let human = new HumanWorker_1.HumanWorker();
human.work();
human.sleep();
let robot = new RobotWorker_1.RobotWorker();
robot.work();
robot.sleep();
//# sourceMappingURL=index.js.map