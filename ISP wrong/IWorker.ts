export interface IWorker{
    work():void;
    sleep():void;
}